from fnmatch import fnmatch
from os.path import join as pjoin
import os
import re
import shutil
import sys

from argparse import ArgumentParser

__title__ = 'spif'
__version__ = '1.1.0'

PICTUREFOLDER = 'scene-{}-p'
VIDEOFOLDER = 'scene-{}-v'
FOLDERS = r'scene-\d+-[PVpv]$'


def main(argv):
    args = parse_command_line(argv)

    if args.version:
        print(name_and_version())
        return 0

    if not args.folder:
        print('No folder specified. Please try {} -h for more information.'.format(__title__))
        return 2

    try:
        if args.reverse:
            recollect(args.folder, args.verbose)
        else:
            spread(args.folder, args.verbose)

    except Exception as excp:
        print(excp)
        return 1

    return 0


def spread(folder, verbose):
    filetype = ''
    number = 1
    for filename in sorted(os.listdir(folder)):
        lowerfilename = filename.lower()
        if fnmatch(lowerfilename, '*.png') or fnmatch(lowerfilename, '*.jpg') \
            or fnmatch(lowerfilename, '*.jpeg') or fnmatch(lowerfilename, '*.bmp'):
            if filetype != 'p':
                filetype = 'p'
                current_folder = PICTUREFOLDER.format(number)

                # Prevent error if folder already exists
                try:
                    os.mkdir(pjoin(folder, current_folder))
                except FileExistsError:
                    pass

                number += 1
        elif fnmatch(lowerfilename, '*.mp4'):
            if filetype != 'v':
                filetype = 'v'
                current_folder = VIDEOFOLDER.format(number)

                # Prevent error if folder already exists
                try:
                    os.mkdir(pjoin(folder, current_folder))
                except FileExistsError:
                    pass

                number += 1
        else:
            continue

        if verbose:
            print(filename, '-->', current_folder)

        shutil.move(pjoin(folder, filename), pjoin(folder, current_folder, filename))


def recollect(folder, verbose):
    p = re.compile(FOLDERS)

    for root, dirs, _ in os.walk(folder):
        for subfolder in dirs:
            m = p.match(subfolder)
            if m:
                subfolder = pjoin(root, subfolder)
                if verbose:
                    print(subfolder)
                for filename in os.listdir(subfolder):
                    if verbose:
                        print('-', filename)
                    shutil.move(pjoin(subfolder, filename), pjoin(folder, filename))

                os.rmdir(subfolder)


def parse_command_line(arguments):
    '''
    Parse the command line arguments

    :param arguments: List of command line arguments
    :return: Arguments object
    '''
    parser = ArgumentParser(prog=__title__,
                            description='SPIF - Separate Pictures (and videos) Into Folders')

    parser.add_argument('folder', nargs='?',
                        help='Start folder to analyse and move files')

    parser.add_argument('--reverse', action='store_true',
                        help='Re-collect files from subfolders to the main folder')

    parser.add_argument('-v', '--verbose',
                        action='store_true',
                        help='Increase verbosity level')

    #=========================================================================
    # parser.add_argument('-n', '--no-action', '--dry-run',
    #                     action='store_true',
    #                     help='Perform no changes, just print what you were doing')
    #=========================================================================

    parser.add_argument('--version',
                        action='store_true',
                        help='Print program name and version number, then exit')

    args = parser.parse_args(arguments)
    return args


def name_and_version():
    return '{pgm} v{version}'.format(
        pgm=__title__,
        version=__version__)


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
