# SPIF - Separate Pictures (and videos) Into Folders

(to be translated into english)

Mit [kdenlive](https://kdenlive.org/) habe ich stets das Problem, dass ich "Diashow-Clips" nur aus einem Ordner bauen lassen kann - es ist keine direkte Auswahl der Dateien möglich. Das bedeutet, dass ich von Hand immer folgendes machen muss:

* Wenn die erste Datei ein Bild ist, Ordner "Szene-1-P" anlegen
* Wenn die erste Datei ein Video ist, Ordner "Szene-1-V" anlegen
* Nun die Datei dort hineinkopieren
* Kopiere die nächste Datei ebenfalls in diesen Ordner, wenn es der selbe Dateityp ist
* Wenn es ein anderer Dateityp ist, beginne von vorn mit der nächsten laufenden Nummer

Nun kann in kdenlive aus den Ordnern "Szene-X-P" jeweils ein Diashow-Clip erstellt und verwendet werden, die Dateien aus "Szene-X-V" können einfach der Reihe nach direkt in die Timeline eingefügt werden.

Leider ist das vorstehend beschriebene mühsam, nervig und in gewisser Weise fehlerträchtig.

Dieses Programm macht das automatisch. Es muss nur der Startordner angegeben werden, alle Dateien darin, die vom Typ Bild oder Video sind, werden entsprechend behandelt, die anderen Dateien werden nicht angefasst.
